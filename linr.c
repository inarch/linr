/*
  Linr: a lightweight parallel computation server.
  Copyright (C) 2011 Stéphane Gimenez.
  Redistribution, modification, and use are permitted.
  As of now, this software comes with no guaranties.
*/

#line 9 "headers.c"

#define _XOPEN_SOURCE 500
#define _GNU_SOURCE

#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <poll.h>
#include <semaphore.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <sys/mman.h>
#include <sched.h>
#include <sys/wait.h>

#define block(code) do { code } while (0)
#define err(fmt, args...) block(                       \
    dbg(1, "error: " fmt, ##args);                     \
    fprintf(stderr, "error: " fmt, ##args);            \
    kill(0, SIGQUIT); exit(1); )
#define wrn(fmt, args...) block(                       \
    dbg(1, fmt, ##args);                               \
    fprintf(stderr, "warning: " fmt, ##args); )
#define inf(fmt, args...) fprintf(stderr, fmt, ##args)

#ifdef DEBUG

#include <time.h>

int debug_level = 2;
FILE *dbgfile = NULL;
char id[16] = "main";
struct timespec dbgtime;

#define fail(fmt, args...) \
    err("failure at %s:%d " fmt, __FILE__, __LINE__, ##args)
#define assert(test) block( if (test); else fail("assertion failed\n"); )
#define dbgf(fmt, args...) block(                                 \
    char path[255];                                               \
    sprintf(id, fmt, ##args); sprintf(path, "log/" fmt, ##args);  \
    dbgfile = fopen(path, "w+"); )
#define dbgc() block( if (dbgfile) fclose(dbgfile); )
#define dbg(i, fmt, args...) block(                               \
    if (i <= DEBUG && i <= debug_level && dbgfile) {              \
      clock_gettime(CLOCK_MONOTONIC, &dbgtime);                   \
      fprintf(dbgfile, "%ld.%09ld %s: " fmt,                      \
       (long)dbgtime.tv_sec, dbgtime.tv_nsec, id, ##args);        \
    } )                                                           \

#else

#define fail(fmt, args...) { __builtin_unreachable(); }
#define assert(test) block( if (test); else __builtin_unreachable(); )
#define dbgf(fmt, args...) {}
#define dbgc() {}
#define dbg(i, fmt, args...) {}

#endif

#line 2 "mem.c"

#ifndef A
#define A __WORDSIZE
#endif
#if A > __WORDSIZE || ! (A == 16 || A == 32 || A == 64)
#error "address size not supported"
#undef A
#define A __WORDSIZE
#endif

#if A == 16
typedef uint16_t maddr;
typedef uint_fast16_t addr;
#define ASHIFT 3 // 3 is max
#define PADDR PRIxFAST16
#elif A == 32
#define ASHIFT 4 // 4 is max
typedef uint32_t maddr;
typedef uint_fast32_t addr;
#define PADDR PRIxFAST32
#elif A == 64
#define ASHIFT 0 // 5 is max, but unnecessary
typedef uint64_t maddr;
typedef uint_fast64_t addr;
#define PADDR PRIxFAST64
#endif
typedef maddr slot[4];
#define SSHIFT (sizeof(slot) >> ASHIFT)

#define PA "%06" PADDR
#define PT "%04" PADDR

const int mem_limit_bits = 8 * sizeof(maddr) + ASHIFT;

uintptr_t mem; //absolute mapped memory base address
uintptr_t mem_size;

struct global
{
  slot slots;
  slot cuts;
  int64_t slot_base;
  int64_t slot_spawn;
  int64_t slot_limit;
  sem_t sem_procs;
  volatile int load;
  //stats
#ifdef DEBUG
  volatile int stat_redex[3];
  volatile int stat_alloc[3];
  volatile int stat_client[3];
  volatile int stat_oid[3];
  volatile int stat_iid[3];
  volatile int stat_bytes_in;
  volatile int stat_bytes_out;
#endif
};

struct global *glb;
const addr glb_slots = 0 * SSHIFT;
const addr glb_cuts = 1 * SSHIFT;

int up;
int procs = 1;

#ifdef DEBUG

int last_spin = 0;
int spin = 0;

int stat_incr(volatile int *stat, int n)
{
  __atomic_add_fetch(&stat[0], n, __ATOMIC_RELAXED);
  int count = __atomic_add_fetch(&stat[2], n, __ATOMIC_RELAXED);
  if (count > stat[1]) stat[1] = count;
  return count;
}
int stat_decr(volatile int *stat, int n)
{
  return __atomic_add_fetch(&stat[2], -n, __ATOMIC_RELAXED);
}

void stats()
{
  inf("\nstats:\n");
  inf("  redexes:          total=%-8d max=%-8d cur=%-8d\n",
      glb->stat_redex[0], glb->stat_redex[1], glb->stat_redex[2]);
  inf("  allocations:      total=%-8d max=%-8d cur=%-8d\n",
      glb->stat_alloc[0], glb->stat_alloc[1], glb->stat_alloc[2]);
  inf("  clients:          total=%-8d max=%-8d cur=%-8d\n",
      glb->stat_client[0], glb->stat_client[1], glb->stat_client[2]);
  inf("  input ids:        total=%-8d max=%-8d cur=%-8d\n",
      glb->stat_iid[0], glb->stat_iid[1], glb->stat_iid[2]);
  inf("  output ids:       total=%-8d max=%-8d cur=%-8d\n",
      glb->stat_oid[0], glb->stat_oid[1], glb->stat_oid[2]);
  inf("  bytes:            in=%-8d    out=%-8d\n",
      glb->stat_bytes_in, glb->stat_bytes_out);
}

#endif

inline maddr *ptr(addr a)
{
  /* if (a >= __builtin_offsetof(struct global, slot_base) &&
     a < sizeof(struct global)) dbg(1, " ! a="PA"\n", a); */
  return (maddr *)(mem + (uintptr_t)(a << ASHIFT));
}

inline void assign(addr a, int o, addr p)
{
  dbg(9, "mem_asg a="PA" o=%d p="PA"\n", a, o, p);
  __atomic_store_n(ptr(a) + o, p, __ATOMIC_RELAXED);
}

inline addr deref(addr a, int o)
{
  addr p = __atomic_load_n(ptr(a) + o, __ATOMIC_RELAXED);
  dbg(9, "mem_drf a="PA" o=%d p="PA"\n", a, o, p);
  return p;
}

inline addr atomic_exchange(addr a, int o, addr p)
{
  addr x = __atomic_exchange_n(ptr(a) + o, (maddr)p, __ATOMIC_ACQUIRE);
  dbg(9, "mem_xch a="PA" o=%d p="PA" x="PA"\n", a, o, p, x);
  return x;
}

inline void atomic_lock(addr a, int o)
{
  maddr expected = 0;
  maddr desired = 1;
#ifdef DEBUG
  last_spin = spin;
#endif
  dbg(9, "mem_lck a="PA" o=%d\n", a, o);
  while (!__atomic_compare_exchange_n(
           ptr(a) + o, &expected, desired,
           0, __ATOMIC_ACQUIRE, __ATOMIC_RELAXED))
  {
    expected = 0;
#ifdef DEBUG
    spin++;
    /* if (spin > 100000) err("overspun"); */
#endif
  }
}

inline void atomic_clear(addr a, int o)
{
  dbg(9, "mem_clr a="PA" o=%d\n", a, o);
  __atomic_clear(ptr(a) + o, __ATOMIC_RELEASE);
}

#line 2 "list.c"

void list_init(addr a)
{
  assign(a, 1, 0);
  assign(a, 2, 0);
  atomic_clear(a, 0);
}

void list_destroy(addr a)
{
}

void list_lock(addr a)
{
  dbg(4, "lock a="PA"\n", a);
  atomic_lock(a, 0);
}

void list_unlock(addr a)
{
  atomic_clear(a, 0);
#ifdef DEBUG
  if (spin == last_spin) {
    dbg(4, "unlock a="PA"\n", a);
  }
  else {
    dbg(4, "unlock a="PA" spin=%d\n", a, spin - last_spin);
  }
#endif
}

void list_push(addr a, addr s)
{
  assign(s, 0, 0);
  list_lock(a);
  addr tail = deref(a, 2);
  if (tail) assign(tail, 0, s);
  else assign(a, 1, s);
  assign(a, 2, s);
  list_unlock(a);
}

addr list_pull(addr a)
{
  list_lock(a);
  addr head = deref(a, 1);
  if (head) {
      addr next = deref(head, 0);
      assign(a, 1, next);
      if (!next) assign(a, 2, 0);
  }
  list_unlock(a);
  return head;
}

#line 2 "reg.c"

#define SLOTM 1024 // max slots
#define SLOT_PACK (SLOTM/2)

int slotn = 0; // slot quantity
addr slotb[SLOTM]; // slot refs buffer
const int slotm = SLOTM; // max quantity
const int slotp = SLOT_PACK; // slot packet size

void slot_spawn()
{
  maddr shift = slotp * SSHIFT;
  addr a = __atomic_fetch_add(&glb->slot_spawn, shift, __ATOMIC_RELAXED);
  dbg(6, "slot_spawn s="PA"\n", a);
  addr limit = glb->slot_limit;
  if (a + shift > limit)
    err("out of memory slots limit="PA"\n", limit); //debug
  for (int i = 0; i < slotp; i++)
    slotb[slotn++] = a + i * SSHIFT;
}

void slot_fill()
{
#if DEBUG
  int islotn = slotn;
#endif
  addr p = list_pull(glb_slots);
  if (!p) { slot_spawn(); return; }
  while (p) { slotb[slotn++] = p; p = deref(p, 1); }
  dbg(4, "slot_fill slots:%d->%d mem=%d\n", islotn, slotn, glb->stat_alloc[2]);
}

void slot_flow()
{
#if DEBUG
  int islotn = slotn;
#endif
  assign(slotb[--slotn], 1, 0);
  for (int i = 1; i < slotp; i++) {
    assign(slotb[slotn - 1], 1, slotb[slotn]);
    slotn--;
  }
  list_push(glb_slots, slotb[slotn]);
  dbg(4, "slot_flow slots:%d->%d mem=%d\n", islotn, slotn, glb->stat_alloc[2]);
}

addr slot_draw()
{
  if (slotn == 0) slot_fill();
  addr p = slotb[--slotn];
#ifdef DEBUG
  int count = stat_incr(glb->stat_alloc, 1);
  dbg(6, "slot_draw s="PA" mem:%d->%d\n", p, count - 1, count);
#endif
  return p;
}

void slot_drop(addr a)
{
  if (slotn >= slotm) slot_flow();
  slotb[slotn++] = a;
#ifdef DEBUG
  int count = stat_decr(glb->stat_alloc, 1);
  dbg(6, "slot_drop s="PA" mem:%d->%d\n", a, count + 1, count);
#endif
}
#line 2 "reg.c"

#define AXPULL 0

#define REGM 64 // max registers < 256

#define CODE_OUT ((maddr)(1))
#define CODE_FWD ((maddr)(2))

#define REG_N 1 // r r r
#define REG_I 2 // a
#define REG_O 3 // a

typedef uint8_t reg_i; //debug
typedef uint8_t ax_i; //debug

typedef union {
  struct { uint8_t arity; uint8_t nid; };
  uint16_t code;
} cell_t;

inline int leveled(cell_t c) { return c.nid >= 64; }

typedef struct {
  uint8_t kind;
  uint8_t level;
  union {
    struct { cell_t cell; ax_i r[3]; };
    maddr a;
  };
} reg;

const int regm = REGM;
int regf = 1;
int regn = 0;
reg regb[REGM];

const int axm = REGM;
int axf = 1;
int axn = 0;
reg_i axb[REGM];

inline reg *rg(reg_i r) { return regb + r; }
inline int rgid(reg_i r) { return r; }
inline int rglv(reg_i r) { return leveled(rg(r)->cell); }

ax_i ax_add(reg_i r)
{
  assert(axf && axf < axm);
  int i = axf;
  axf = axb[axf];
  axn++;
  dbg(8, "ax_add i=%02x axs:%d->%d\n", i, axn - 1, axn);
  axb[i] = r;
  return i;
}

void ax_del(ax_i i)
{
  axb[i] = axf;
  axf = i;
  axn--;
  dbg(8, "ax_del i=%02x axs:%d->%d\n", i, axn + 1, axn);
}

reg_i reg_add()
{
  reg_i r = regf;
  assert(r && r < regm);
  regf = regb[regf].level;
  regn++;
  dbg(8, "reg_add r=%02x regs:%d->%d\n", rgid(r), regn - 1, regn);
  assert(!rg(r)->kind);
  return r;
}

void reg_del(reg_i r)
{
  assert(rg(r)->kind);
  rg(r)->kind = 0;
  rg(r)->level = regf;
  regf = r;
  regn--;
  dbg(8, "reg_del r=%02x regs:%d->%d\n", rgid(r), regn + 1, regn);
}

void reg_print(reg_i r)
{
  if (rg(r)->kind == REG_N)
    switch (rg(r)->cell.arity) {
    case 0: dbg(8, "r=%02x: N%d[%02d]\n", rgid(r), 0, rg(r)->cell.nid); break;
    case 1: dbg(8, "r=%02x: N%d[%02d] %02x\n", rgid(r), 1, rg(r)->cell.nid, rg(r)->r[0]); break;
    case 2: dbg(8, "r=%02x: N%d[%02d] %02x %02x\n", rgid(r), 2, rg(r)->cell.nid, rg(r)->r[0], rg(r)->r[1]); break;
    default: dbg(8, "r=%02x: N%d[%02d] %02x %02x %02x\n", rgid(r), rg(r)->cell.arity, rg(r)->cell.nid, rg(r)->r[0], rg(r)->r[1], rg(r)->r[2]);
    }
  if (rg(r)->kind == REG_I) dbg(8, "r=%02x: I "PA"\n", rgid(r), rg(r)->a);
  if (rg(r)->kind == REG_O) dbg(8, "r=%02x: O "PA"\n", rgid(r), rg(r)->a);
}

void reg_format()
{
  for (int i = 0 ; i < axm; i++)
    axb[i] = i + 1;
  axb[axm - 1] = 0;
  for (int i = 0 ; i < regm; i++) {
    regb[i].kind = 0;
    regb[i].level = i + 1;
  }
  regb[regm - 1].level = 0;
}

reg_i reg_cpy(reg_i r)
{
  reg_i n = reg_add();
  *rg(n) = *rg(r);
  return n;
}

reg_i reg_cell()
{
  reg_i r = reg_add();
  rg(r)->kind = REG_N;
  return r;
}

reg_i reg_in(addr s)
{
  reg_i r = reg_add();
  rg(r)->kind = REG_I; rg(r)->a = s;
#if AXPULL
  *(ptr(s) + 1) = r;
#endif
  return r;
}

reg_i reg_out(addr s)
{
  reg_i r = reg_add();
  rg(r)->kind = REG_O; rg(r)->a = s;
  return r;
}

void cut_new(reg_i r1, reg_i r2);

void ax_cast(reg_i r, ax_i i)
{
  dbg(7, "ax_cast r=%02x i=%02x\n", rgid(r), i);
  assert(r && i);
  reg_i r2 = axb[i];
  if (r2) { ax_del(i); cut_new(r2, r); }
  else axb[i] = r;
}

void ax_join(ax_i i1, ax_i i2)
{
  dbg(7, "ax_join i1=%02x i2=%02x\n", i1, i2);
  assert(i1 && i2);
  reg_i r1 = axb[i1];
  reg_i r2 = axb[i2];
  if (r1 && r2) { ax_del(i1); ax_del(i2); cut_new(r1, r2); }
  else if (r1) { axb[i2] = r1; ax_del(i1); }
  else if (r2) { axb[i1] = r2; ax_del(i2); }
  else fail("todo\n"); // forward
}


void reg_pull(addr a, reg_i r);

// todo: pull axioms ugliness
void ax_pull(addr a, reg_i r)
{
#if AXPULL
  reg_i ri = *(ptr(a) + 1);
  if (ri && rg(ri)->kind == REG_I && ri != r && rg(ri)->a == a) {
    dbg(7, "reg_a_pull r1=%02x r2=%02x\n", rgid(r), rgid(ri));
    rg(ri)->kind = REG_A; rg(ri)->r[0] = r;
    rg(r)->kind = REG_A; rg(r)->r[0] = ri;
    slot_drop(a);
  }
#endif
  addr x = atomic_exchange(a, 0, 0);
  if (x) {
    dbg(7, "reg_s_pull c="PA"\n", a);
    slot_drop(a);
    reg_pull(x, r);
  }
  else {
    dbg(7, "reg_i_pull r=%02x s="PA"\n", rgid(r), a);
    rg(r)->kind = REG_I; rg(r)->a = a;
#if AXPULL
    *(ptr(a) + 1) = (addr)r;
#endif
  }
}

void reg_pull(addr a, reg_i r)
{
  addr code = deref(a, 0);
  if (code == CODE_FWD)
  {
    dbg(7, "reg_f_pull c="PA"\n", a);
    addr sf = deref(a, 1);
    slot_drop(a);
    ax_pull(sf, r);
  }
  else if (code == CODE_OUT)
  {
    dbg(7, "reg_o_pull r=%02x c="PA"\n", rgid(r), a);
    rg(r)->kind = REG_O; rg(r)->a = a;
  }
  else
  {
    rg(r)->kind = REG_N;
    rg(r)->cell.code = code;
    dbg(7, "reg_n_pull r=%02x[%02d] c="PA"\n", rgid(r), rg(r)->cell.nid, a);
    assert(rg(r)->cell.nid);
    assert(rg(r)->cell.arity <= 3);
    for (int p = 0; p < rg(r)->cell.arity; p++)
      rg(r)->r[p] = ax_add(reg_in(deref(a, p + 1)));
    if (rglv(r)) rg(r)->level = deref(a, 3);
    slot_drop(a);
  }
}

void reg_probe(reg_i r)
{
  if (rg(r)->kind == REG_I)
    ax_pull(rg(r)->a, r);
}


int reg_push(reg_i r, addr *ap);

addr ax_push(ax_i i)
{
  dbg(7, "ax_push i=%02x\n", i);
  reg_i r = axb[i];
  if (r) {
    ax_del(i);
    addr a;
    if (reg_push(r, &a)) return a;
    else {
      addr s = slot_draw();
      bzero(ptr(s), sizeof(slot));
      assign(s, 0, a);
      return s;
    }
  }
  else {
    addr s = slot_draw();
    bzero(ptr(s), sizeof(slot));
    axb[i] = reg_in(s);
    return s;
  }
}

int reg_i_push(reg_i ri, addr *ap)
{
  addr s = rg(ri)->a;
  dbg(7, "reg_i_push r=%02x s="PA"\n", rgid(ri), s);
  reg_del(ri);
  *ap = s;
  return 1;
}

int reg_n_push(reg_i rn, addr *ap)
{
  dbg(7, "reg_n_push r=%02x[%02d]\n", rgid(rn), rg(rn)->cell.nid);
  addr c = slot_draw();
  assign(c, 0, rg(rn)->cell.code);
  assert(rg(rn)->cell.arity <= 3);
  for (int p = 0; p < rg(rn)->cell.arity; p++)
    assign(c, p + 1, ax_push(rg(rn)->r[p]));
  if (rglv(rn)) assign(c, 3, rg(rn)->level);
  reg_del(rn);
  *ap = c;
  return 0;
}

int reg_o_push(reg_i ro, addr *ap)
{
  addr c = rg(ro)->a;
  dbg(7, "reg_o_push r=%02x c="PA"\n", rgid(ro), c);
  reg_del(ro);
  *ap = c;
  return 0;
}

int reg_push(reg_i r, addr *ap)
{
  if (rg(r)->kind == REG_I) return reg_i_push(r, ap);
  if (rg(r)->kind == REG_N) return reg_n_push(r, ap);
  if (rg(r)->kind == REG_O) return reg_o_push(r, ap);
  fail("kind=%d\n", rg(r)->kind);
}
#line 2 "cut.c"

pid_t signal_pid = 0;
int signal_count = 0;

#define CUTM 32 // max cut registers

const int cutm = CUTM;
int cutn = 0;
reg_i cutb[CUTM][2];

void cut_kill()
{
  if (signal_pid != 0) {
    dbg(2, "signaled\n");
    kill(signal_pid, SIGIO);
    signal_pid = 0;
  }
}

void cut_signal(addr client)
{
  pid_t pid = *(pid_t *)(ptr(client) + 2);
  if (signal_pid != pid) cut_kill();
  signal_pid = pid;
  signal_count = 0;
}

void cut_add(reg_i r1, reg_i r2)
{
  assert(cutn < cutm);
  cutb[cutn][0] = r1;
  cutb[cutn][1] = r2;
  cutn++;
}

void cut_new(reg_i r1, reg_i r2)
{
  assert(r1 && r2);
  cut_add(r1, r2);
#ifdef DEBUG
  int count = stat_incr(glb->stat_redex, 1);
#endif
  dbg(7, "cut_new r1=%02x r2=%02x cuts:%d->%d cutg:%d->%d\n",
      rgid(r1), rgid(r2), cutn - 1, cutn, count - 1, count);
}

int cut_pop(reg_i *r1, reg_i *r2)
{
  if (cutn == 0) return 0;
  cutn--;
  *r1 = cutb[cutn][0];
  *r2 = cutb[cutn][1];
#ifdef DEBUG
  int count = stat_decr(glb->stat_redex, 1);
#endif
  dbg(7, "cut_pop r1=%02x r2=%02x cuts:%d->%d cutg:%d->%d\n",
      rgid(*r1), rgid(*r2), cutn + 1, cutn, count + 1, count);
  return 1;
}

int out_pull(addr a, addr *s1, addr *s2)
{
  addr sr = list_pull(a);
  if (sr) {
    *s1 = deref(sr, 1);
    *s2 = deref(sr, 2);
    dbg(6, "out_pull a="PA" s1="PA" s2="PA"\n", a, *s1, *s2);
    slot_drop(sr);
    return 1;
  }
  return 0;
}

void out_push(addr c1, addr c2)
{
  addr client = deref(c1, 1);
  addr s = slot_draw();
  assign(s, 1, c1);
  assign(s, 2, c2);
  addr a = deref(client, 1);
  list_push(a, s);
  dbg(5, "out_push a="PA" c1="PA" c2="PA"\n", a, c1, c2);
  cut_signal(client);
}

void cut_push(addr c1, addr c2);

void cut_cast(addr c, addr s)
{
  addr x = atomic_exchange(s, 0, c);
  dbg(6, "cut_cast s="PA" c="PA" cx="PA"\n", s, c, x);
  if (x) { slot_drop(s); cut_push(c, x); }
}

void cut_join(addr s1, addr s2)
{
  addr x1 = atomic_exchange(s1, 0, 0);
  addr x2 = atomic_exchange(s2, 0, 0);
  dbg(6, "cut_join s1="PA" s2="PA"\n", s1, s2);
  if (x1 && x2) { slot_drop(s1); slot_drop(s2); cut_push(x1, x2); }
  else if (x1) { slot_drop(s1); cut_cast(x1, s2); }
  else if (x2) { slot_drop(s2); cut_cast(x2, s1); }
  else {
    if (1) { // asymetric forward
      addr s = slot_draw();
      assign(s, 0, CODE_FWD);
      assign(s, 1, s2);
      cut_cast(s, s1);
    }
    else { // symetric forward
      addr s = slot_draw();
      assign(s, 0, 0);
      addr cf1 = slot_draw();
      addr cf2 = slot_draw();
      assign(cf1, 0, CODE_FWD);
      assign(cf1, 1, s);
      assign(cf2, 0, CODE_FWD);
      assign(cf2, 1, s);
      cut_cast(cf1, s1);
      cut_cast(cf2, s2);
    }
  }
}

void cut_drop(reg_i r1, reg_i r2)
{
  dbg(6, "cut_drop r1=%02x r2=%02x\n", rgid(r1), rgid(r2));
  addr a1, a2;
  int sync1 = reg_push(r1, &a1);
  int sync2 = reg_push(r2, &a2);
  if (sync1 && sync2) cut_join(a1, a2);
  else if (sync1) cut_cast(a2, a1);
  else if (sync2) cut_cast(a1, a2);
  else cut_push(a1, a2);
}

void cut_push(addr c1, addr c2)
{
  addr code1 = deref(c1, 0);
  addr code2 = deref(c2, 0);
  if (code1 == CODE_FWD && code2 == CODE_FWD)
  { cut_join(deref(c1, 1), deref(c2, 1)); slot_drop(c1); slot_drop(c2); }
  else if (code1 == CODE_FWD) { cut_cast(c2, deref(c1, 1)); slot_drop(c1); }
  else if (code2 == CODE_FWD) { cut_cast(c1, deref(c2, 1)); slot_drop(c2); }
  else if (code1 == CODE_OUT) out_push(c1, c2);
  else if (code2 == CODE_OUT) out_push(c2, c1);
  else {
    addr s = slot_draw();
    assign(s, 1, c1);
    assign(s, 2, c2);
    list_push(glb_cuts, s);
#ifdef DEBUG
    int count = stat_incr(glb->stat_redex, 1);
#endif
    int load = __atomic_fetch_add(&glb->load, 1, __ATOMIC_RELAXED);
    dbg(5, "cut_push cuts=%d cutg:%d->%d\n", cutn, count, count + 1);
    if (load < 0) sem_post(&glb->sem_procs);
  }
}

void cut_fill(int n)
{
  while (cutn < n) {
    int load = __atomic_fetch_add(&glb->load, -1, __ATOMIC_RELAXED);
    if (load <= 0) {
      dbg(3, "---\n");
      cut_kill();
      while (sem_wait(&glb->sem_procs)) if (!up) return;
      dbg(3, "+++\n");
    }
    dbg(5, "cut_fill cuts:%d->%d cutg=%d\n", cutn, cutn + 1,
        glb->stat_redex[2]);
    addr s = list_pull(glb_cuts);
    assert(s);
    addr a1 = deref(s, 1);
    addr a2 = deref(s, 2);
    reg_i r1 = reg_add();
    reg_i r2 = reg_add();
    reg_pull(a1, r1);
    reg_pull(a2, r2);
    cut_add(r1, r2);
    slot_drop(s);
  }
}

void cut_flow(int n)
{
  while (cutn > n) {
    reg_i r1, r2;
    cut_pop(&r1, &r2);
    cut_drop(r1, r2);
  }
}
#line 2 "eng.c"

uint64_t engine_formatid = 0x00000000726e696c; // "linr"
uint64_t engine_version = 0;

struct engine_s
{
  uint64_t formatid;
  uint64_t version;
  uint64_t eng_table_offset;
  uint64_t eng_rules_offset;
  uint64_t eng_kinds;
  uint64_t eng_rules;
  uint64_t eng_shift;
  uint64_t unused;
};
typedef struct engine_s engine_t;

engine_t engine;
uintptr_t eng_ptr;
uintptr_t eng_size;
uintptr_t eng_table_ptr;
uintptr_t eng_rules_ptr;

void eng_mcross(reg_i r1, reg_i r2)
{
  dbg(6, "eng_mcross\n");
  assert(rg(r1)->cell.arity <= 2);
  assert(rg(r2)->cell.arity <= 3);
  int arity1 = rg(r1)->cell.arity;
  reg_i p11 = 0, p12 = 0;
  if (arity1 > 0) p11 = reg_cpy(r2);
  if (arity1 > 1) p12 = reg_cpy(r2);
  for (int p = 0; p < rg(r2)->cell.arity; p++) {
    reg_i p2 = reg_cpy(r1);
    if (arity1 > 0) rg(p11)->r[p] = rg(p2)->r[0] = ax_add(0);
    if (arity1 > 1) rg(p12)->r[p] = rg(p2)->r[1] = ax_add(0);
    ax_cast(p2, rg(r2)->r[p]);
  }
  if (arity1 > 0) ax_cast(p11, rg(r1)->r[0]);
  if (arity1 > 1) ax_cast(p12, rg(r1)->r[1]);
}

void eng_mjoin(reg_i r1, reg_i r2)
{
  dbg(6, "eng_mjoin\n");
  assert(rg(r1)->cell.arity <= 2);
  assert(rg(r1)->cell.nid == rg(r2)->cell.nid);
  for (int p = 0; p < rg(r1)->cell.arity; p++)
    ax_join(rg(r1)->r[p], rg(r2)->r[p]);
}

#define LAX 1
#define LPR 2
#define LAU 3 // up to 6
struct look_s { uint8_t p; reg_i r; } lookt[16];
void eng_port(int i, uint8_t p1, reg_i r1)
{
  if (!lookt[i].p) { lookt[i].p = p1; lookt[i].r = r1; return; }
  uint8_t p2 = lookt[i].p;
  reg_i r2 = lookt[i].r;
  assert(p1 <= 6 && p2 <= 6);
  if (p1 == LPR && p2 == LPR) cut_new(r1, r2);
  else if (p1 == LPR && p2 == LAX) ax_cast(r1, r2);
  else if (p1 == LAX && p2 == LPR) ax_cast(r2, r1);
  else if (p1 == LAX) rg(r2)->r[p2 - LAU] = r1;
  else if (p2 == LAX) rg(r1)->r[p1 - LAU] = r2;
  else if (p1 == LPR) rg(r2)->r[p2 - LAU] = ax_add(r1);
  else if (p2 == LPR) rg(r1)->r[p1 - LAU] = ax_add(r2);
  else rg(r1)->r[p1 - LAU] = rg(r2)->r[p2 - LAU] = ax_add(0);
}

void eng_table(reg_i r1, reg_i r2)
{
  uint8_t nid1 = rg(r1)->cell.nid - 1;
  uint8_t nid2 = rg(r2)->cell.nid - 1;
  int index = (nid1 << engine.eng_shift) + nid2;
  uint_fast16_t offset = *((uint16_t *)eng_table_ptr + index);
  if (offset == (uint16_t)-1)
    err("reduction error k1=%d k2=%d\n", nid1, nid2);
  uint8_t *pc = (uint8_t *)(eng_rules_ptr + offset);
  uint8_t *pc_end = pc + pc[0];
  pc++;
  bzero(lookt, sizeof(lookt));
  int i = 0;
  assert(rg(r1)->cell.arity <= 3);
  assert(rg(r2)->cell.arity <= 3);
  for (int p = 0; p < rg(r1)->cell.arity; p++)
    { lookt[i].p = LAX; lookt[i++].r = rg(r1)->r[p]; }
  for (int p = 0; p < rg(r2)->cell.arity; p++)
    { lookt[i].p = LAX; lookt[i++].r = rg(r2)->r[p]; }
  dbg(7, "eng_redex ports=%d\n", i);
  while (pc < pc_end) {
    if (pc[0] == 0) {
      dbg(6, "eng_link n1=%d n2=%d\n", pc[1], pc[2]);
      ax_join(lookt[pc[1]].r, lookt[pc[2]].r);
      pc += 3;
    }
    else {
      reg_i r = reg_cell();
      rg(r)->cell.arity = pc[1] & 3;
      rg(r)->cell.nid = pc[0];
      rg(r)->level = 0;
      dbg(6, "eng_cell r=%02x[%02d]\n", rgid(r), rg(r)->cell.nid);
      assert(rg(r)->cell.arity <= 3);
      for (int p = 0; p < rg(r)->cell.arity; p++)
        eng_port(pc[p + 3], p + LAU, r);
      eng_port(pc[2], LPR, r);
      pc += rg(r)->cell.arity + 3;
    }
  }
}

void reduce(reg_i r1, reg_i r2)
{
  dbg(5, "eng_reduce r1=%02x[%02d] r2=%02x[%02d]\n",
      rgid(r1), rg(r1)->cell.nid, rgid(r2), rg(r2)->cell.nid);
  assert(rg(r1)->cell.nid && rg(r2)->cell.nid);
  if (rglv(r1) && rglv(r2)) {
    if (rg(r1)->level > rg(r2)->level) eng_mcross(r1, r2);
    else if (rg(r1)->level < rg(r2)->level) eng_mcross(r2, r1);
    else eng_mjoin(r1, r2);
  }
  else if (rglv(r1)) eng_mcross(r1, r2);
  else if (rglv(r2)) eng_mcross(r2, r1);
  else {
    if (rg(r1)->cell.nid < rg(r2)->cell.nid) eng_table(r1, r2);
    else eng_table(r2, r1);
  }
  reg_del(r1);
  reg_del(r2);
}

void compute()
{
  while (up) {
    reg_i r1, r2;
    if (signal_count++ > 10) cut_kill();
    if (!cut_pop(&r1, &r2)) { cut_fill(1); continue; }
    reg_probe(r1); reg_probe(r2);
    reg_print(r1); reg_print(r2);
    /* if (rg(r1)->kind == REG_A || rg(r2)->kind == REG_A) */
      /* reg_join(r1, r2); */
    if (rg(r1)->kind == REG_N && rg(r2)->kind == REG_N) {
      if (cutn < cutm - 16 && regn < regm - 16) // hardcoded
        reduce(r1, r2);
      else {
        dbg(5, "eng_pool cuts=%02d/%d regs=%02d/%d axs=%02d/%d\n",
            cutn, cutm, regn, regm, axn, axm);
        cut_drop(r1, r2);
      }
    }
    else
      cut_drop(r1, r2);
  }
}
#line 2 "map.c"

// todo: search trees

void map_init(addr a)
{
  assign(a, 3, 0);
}

void map_destroy(addr a)
{
}

addr map_get(addr a, addr i)
{
  addr p = a;
  a = deref(p, 3);
  while (a != 0) {
    if (deref(a, 2) == i) {
      assign(p, 3, deref(a, 3));
      dbg(5, "map_out id=%d s="PA"\n", (int)i, a);
      return a;
    }
    p = a;
    a = deref(p, 3);
  }
  return 0;
}

addr map_put(addr a, addr i)
{
  addr s = slot_draw();
  bzero(ptr(s), sizeof(slot));
  assign(s, 2, i);
  assign(s, 3, deref(a, 3));
  assign(a, 3, s);
  dbg(5, "map_in id=%d s="PA"\n", (int)i, s);
  return s;
}

addr map_bind(addr a, addr i)
{
  addr c = map_get(a, i);
  if (c) {
#ifdef DEBUG
      stat_decr(glb->stat_iid, 1);
#endif
      return c;
  }
  else {
#ifdef DEBUG
      stat_incr(glb->stat_iid, 1);
#endif
      return map_put(a, i);
  }
}

#line 2 "serve.c"

const int server_version = 1;
char *server_path = NULL;
int server_client_max = 512;
int server_listen_queue = 16;
int server_buffers_size = 4096;

struct client_s
{
  int id;
  int fd;
  int r, w;
  unsigned char *buf_r, *buf_w;
  int ri, wi;
  int ic, oc; // inputs and outputs count
  addr s; // client slot
  addr map; // cut slot
  addr cuts; // cut slot
  addr name_count;
};
typedef struct client_s client_t;

struct clientp_s
{
  struct clientp_s *next;
  client_t *this;
};
typedef struct clientp_s clientp_t;

int sfd = -1;
clientp_t *clientp;
clientp_t *clientp_free;
clientp_t *clientp_first;
struct pollfd *ps;

addr new_output(client_t *c, addr s)
{
  addr arg = c->name_count;
  c->name_count += 2;
  addr co = slot_draw();
  assign(co, 0, CODE_OUT);
  assign(co, 1, c->s);
  assign(co, 2, arg);
  cut_cast(co, s);
  c->oc += 1;
#ifdef DEBUG
  stat_incr(glb->stat_oid, 1);
#endif
  return arg;
}

void read_bufs(client_t *c)
{
  int i = 0;
  while (i < c->ri) {
    unsigned char *p = c->buf_r + i;
    int length = p[0];
    if (length < 2) break; // protocol error
    if (length > 64) err("unhandled\n");
    if (i + length > c->ri) break; // not enough data
    addr args[5] = {};
    int nargs = 0;
    int shift = 0;
    for (int x = 1; x < length; x++) {
      if (nargs >= 5) err("unhandled\n");
      args[nargs] += (p[x] & 127) << shift;
      if (p[x] < 128) { nargs++; shift = 0; }
      else shift += 7;
    }
#ifdef DEBUG
    char buf[64], *bufp = buf;
    for (int k = 0; k < nargs; k++)
      bufp += snprintf(bufp, 63, "%02d:", (int)args[k]);
    if (bufp > buf) *(bufp - 1) = 0;
    dbg(2, "c%d read data=%s\n", c->fd, buf);
#endif
    if (args[0] == 0) {
      if (args[1] == 0) { // link
        addr s1 = map_bind(c->map, args[2]);
        addr s2 = map_bind(c->map, args[3]);
        cut_join(s1, s2);
      }
      if (args[1] == 1) { // get
        addr s = map_bind(c->map, args[3]);
        addr co = slot_draw();
        assign(co, 0, CODE_OUT); // output
        assign(co, 1, c->s);
        assign(co, 2, args[2]);
        cut_cast(co, s);
        c->oc++;
#ifdef DEBUG
        stat_incr(glb->stat_oid, 1);
#endif
      }
    }
    else { // node
      cell_t cell; cell.arity = nargs - 2; cell.nid = args[0];
      assert(cell.arity <= 3);
      addr s = map_bind(c->map, args[1]);
      addr cn = slot_draw();
      assign(cn, 0, cell.code);
      for (int p = 0; p < cell.arity; p++)
        assign(cn, p + 1, map_bind(c->map, args[p + 2]));
      if (leveled(cell)) assign(cn, 3, args[nargs - 1]);
      cut_cast(cn, s);
    }
    i += length;
  }
  c->ri -= i;
  if (c->ri) memmove(c->buf_r, c->buf_r + i, c->ri);
}

void write_bufs(client_t *c)
{
  while (c->wi + 64 <= server_buffers_size) { // ensures enough buffer space
    addr s1, s2;
    if (!out_pull(c->cuts, &s1, &s2)) break;
    assert(deref(s1, 1) == c->s);
    addr args[5] = { 0 };
    int nargs;
    uint16_t code = deref(s2, 0);
    if (code == CODE_OUT) { // link
      assert(deref(s2, 1) == c->s);
      dbg(5, "out_redex c="PA" link\n", c->s);
      args[0] = 0;
      args[1] = 0;
      args[2] = deref(s1, 2);
      args[3] = deref(s2, 2);
      nargs = 4;
      c->oc -= 2;
#ifdef DEBUG
      stat_decr(glb->stat_oid, 2);
#endif
    }
    else { // node
      cell_t cell; cell.arity = 0; cell.nid = 0;
      cell.code = code;
      dbg(5, "out_redex c="PA" k=%d\n", c->s, cell.nid);
      args[0] = cell.nid;
      args[1] = deref(s1, 2);
      for (int p = 0; p < cell.arity; p++)
        args[2 + p] = new_output(c, deref(s2, 1 + p));
      nargs = 2 + cell.arity;
      if (leveled(cell)) args[nargs++] = deref(s2, 3);
      c->oc -= 1;
#ifdef DEBUG
      stat_decr(glb->stat_oid, 1);
#endif
    }
    slot_drop(s1);
    slot_drop(s2);
#ifdef DEBUG
    char buf[64], *bufp = buf;
    for (int k = 0; k < nargs; k++)
      bufp += snprintf(bufp, 63, "%02d:", (int)args[k]);
    if (bufp > buf) *(bufp - 1) = 0;
    dbg(2, "c%d write data=%s\n", c->fd, buf);
#endif
    unsigned char *p = c->buf_w + c->wi;
    int i = 1;
    for (int k = 0; k < nargs; k++) {
      while (args[k] > 127) {
        p[i++] = 128 + (args[k] & 127);
        args[k] = args[k] >> 7;
      }
      p[i++] = args[k];
    }
    if (i > 64) err("unhandled\n");
    p[0] = i;
    c->wi += i;
  }
}

client_t *add_client(int fd)
{
  dbg(1, "add c%d\n", fd);
  client_t *c = (client_t *)malloc(sizeof(client_t));
  c->fd = fd;
  c->r = 1; c->w = 1;
  c->ri = 0; c->wi = 0;
  c->oc = 0;
  c->name_count = 1;
  c->buf_r = (unsigned char *)malloc(server_buffers_size);
  c->buf_w = (unsigned char *)malloc(server_buffers_size);
  c->map = slot_draw(); map_init(c->map);
  c->cuts = slot_draw(); list_init(c->cuts);
  c->s = slot_draw();
  assign(c->s, 0, c->map);
  assign(c->s, 1, c->cuts);
  *(pid_t *)(ptr(c->s) + 2) = getpid(); // pid
#ifdef DEBUG
  stat_incr(glb->stat_client, 1);
#endif
  dbg(1, "added c%d a="PA"\n", fd, c->s); //debug
  return c;
}

void del_client(client_t *c)
{
  dbg(1, "del c%d\n", c->fd);
  close(c->fd);
  free(c->buf_r); free(c->buf_w);
  map_destroy(c->map); slot_drop(c->map);
  list_destroy(c->cuts); slot_drop(c->cuts);
  slot_drop(c->s);
  free(c);
#ifdef DEBUG
  stat_decr(glb->stat_client, 1);
#endif
}

void do_recv(client_t *c)
{
  dbg(3, "c%d receiving...\n", c->fd);
  int ret = recv(c->fd, c->buf_r + c->ri, server_buffers_size - c->ri, 0);
  if (ret > 0) {
    c->ri += ret;
#ifdef DEBUG
    glb->stat_bytes_in += ret;
    dbg(3, "c%d received %d\n", c->fd, ret);
#endif
  }
  else if (ret == 0) c->r = 0;
  else if (ret == -1 && errno != EAGAIN) {
    dbg(1, "c%d recv error %d\n", c->fd, errno);
    c->r = 0;
  }
}

void do_send(client_t *c)
{
  dbg(3, "c%d sending %d...\n", c->fd, c->wi);
  int ret = send(c->fd, c->buf_w, c->wi, 0);
  if (ret >= 0) {
    if (c->wi > server_buffers_size - 64) raise(SIGIO);
    c->wi -= ret;
#ifdef DEBUG
    glb->stat_bytes_out += ret;
    dbg(3, "c%d sent %d\n", c->fd, ret);
#endif
    if (c->wi) memmove(c->buf_w + ret, c->buf_w, c->wi);
  }
  else if (ret == 0) c->w = 0;
  else if (ret == -1 && errno != EAGAIN) {
    dbg(1, "c%d send error %d\n", c->fd, errno);
    c->w = 0;
  }
}

void socket_async(int fd)
{
  int flag;
  if (fcntl(fd, F_SETOWN, getpid()) == -1 ||
      (flag = fcntl(fd, F_GETFL, 0)) == -1 ||
      fcntl(fd, F_SETFL, flag | O_NONBLOCK | O_ASYNC) == -1)
    err("failed to set socket parameters, errno=%d\n", errno);
}

void io()
{
  int n = 1;
  for (clientp_t *p = clientp_first; p; p = p->next) {
    client_t *c = p->this;
    ps[n].fd = c->fd;
    ps[n].events = POLLERR;
    if (c->r) ps[n].events |= POLLIN;
    if (c->w && c->wi) ps[n].events |= POLLOUT;
    n++;
  }
  if (poll(ps, n, 0) != -1) {
    clientp_t **p = &clientp_first;
    for (int i = 1; *p; i++) {
      clientp_t *cp = *p;
      client_t *c = cp->this;
      dbg(3, "c%d polled outputs=%d\n", c->fd, c->oc);
      if (ps[i].revents & POLLIN) do_recv(c);
      if (ps[i].revents & POLLOUT) do_send(c);
      if (ps[i].revents & POLLERR) {
        c->w = 0;
        dbg(1, "c%d error\n", c->fd);
      }
      if (c->oc == 0 && (c->w == 0 || (c->r == 0 && c->wi == 0))) {
        del_client(c);
        *p = cp->next;
        cp->next = clientp_free;
        clientp_free = cp;
      }
      else p = &(*p)->next;
    }
    if (ps[0].revents & POLLIN) {
      if (clientp_free) {
        int s = accept(sfd, NULL, NULL);
        if (s == -1) wrn("accept failed, errno=%d\n", errno);
        else {
          socket_async(s);
          clientp_t *new = clientp_free;
          clientp_free = clientp_free->next;
          new->this = add_client(s);
          new->this->id = new - clientp;
          new->next = clientp_first;
          clientp_first = new;
          do_recv(new->this);
        }
      }
      else wrn("maximum connections reached (%d)", server_client_max);
    }
  }
  else if (errno != EINTR && errno != EAGAIN)
    wrn("poll failed, errno=%d\n", errno);
}

void io_handler(int sig)
{
  dbg(2, "io signal\n");
}

int setup_socket()
{
  struct sockaddr_un addr;
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, server_path, sizeof(addr.sun_path) - 1);
  int sfd = socket(PF_UNIX, SOCK_STREAM, 0);
  if (sfd == -1)
    err("failed to allocate server socket, errno=%d\n", errno);
  socket_async(sfd);
  if (bind(sfd, (struct sockaddr *)&addr, sizeof(struct sockaddr_un)) == -1)
    err("failed to bind %s, errno=%d\n", server_path, errno);
  ps[0].fd = sfd;
  ps[0].events = POLLIN;
  return sfd;
}

void server()
{
  ps = malloc((server_client_max + 1) * sizeof(struct pollfd));
  clientp = malloc(server_client_max * sizeof(clientp_t));
  int i;
  for (i = 0; i < server_client_max - 1; i++)
    clientp[i].next = &clientp[i+1];
  clientp[i].next = NULL;
  clientp_free = clientp;
  clientp_first = NULL;

  sfd = setup_socket();

  sigset_t sigempty_set;
  sigemptyset(&sigempty_set);
  sigset_t sigio_set;
  sigemptyset(&sigio_set);
  sigaddset(&sigio_set, SIGIO);
  struct sigaction siga;
  siga.sa_flags = 0;
  sigemptyset(&siga.sa_mask);
  siga.sa_handler = io_handler;
  sigaction(SIGIO, &siga, NULL);
  sigprocmask(SIG_BLOCK, &sigio_set, NULL);

  if (listen(sfd, server_listen_queue) == -1)
    err("cannot listen, errno=%d\n", errno);

  while (sigsuspend(&sigempty_set), up) {
    dbg(3, "+++\n");
    for (clientp_t *p = clientp_first; p; p = p->next) write_bufs(p->this);
    dbg(2, "io <<<\n"); io(); dbg(2, "io >>>\n");
    for (clientp_t *p = clientp_first; p; p = p->next) read_bufs(p->this);
    cut_kill();
    dbg(3, "---\n");
  }
}

void server_down()
{
  free(ps);
  free(clientp);
  if (sfd >= 0) {
    close(sfd);
    if (unlink(server_path) == -1)
      wrn("failed to unlink server socket, errno=%d\n", errno);
  }
}
#line 2 "run.c"

char *engine_path = NULL;

uint64_t mem_min = 0;
uint64_t mem_max = 256 * 1024 * sizeof(slot);
uint64_t mem_limit = 0;
uint64_t mem_pagesize = 0;

void sigh(int sig)
{
  dbg(1, "signal %d\n", sig);
#ifdef DEBUG
  if (dbgfile) fflush(dbgfile);
#endif
  switch (sig) {
  case SIGTERM: case SIGINT: case SIGQUIT: up = 0; break;
  case SIGTSTP: raise(SIGSTOP); break;
  case SIGSEGV: exit(-sig);
  }
}

void workers()
{
  fflush(stdout);
  fflush(stderr);
  for (int k = 1; k <= procs; k++)
    if (fork() == 0) {
      dbgf("p%d", k);
      compute();
      dbgc();
      exit(0);
    }
}

void help(char *bin)
{
  inf("usage: %s engine socket\n"
      "options:\n"
#ifdef DEBUG
      " -d=%-8d debug level\n"
#endif
      " -p=%-8d number of worker processes\n"
      " -m=%-8"PRIu64" upper memory limit, in KiB\n"
      " -c=%-8d network connection limit\n"
      " -b=%-8d network buffers size\n"
      " -q=%-8d network listen queue size\n",
      bin,
#ifdef DEBUG
      debug_level,
#endif
      procs, mem_max / 1024,
      server_client_max, server_buffers_size, server_listen_queue
      );
}

inline char *arg(char *a)
{
  if (a[2] != '=') err("invalid options\n");
  return a + 3;
}

void parse(char **argv)
{
  char **c = argv;
  while (*(++c) != NULL) {
    if ((*c)[0] != '-') {
      if (!engine_path) engine_path = *c;
      else if (!server_path) server_path = *c;
      else err("invalid options\n");
    }
    else
      switch ((*c)[1]) {
      case 'h':
        if ((*c)[2] != '\0') err("invalid options\n");
        help(argv[0]); exit(0);
        break;
#ifdef DEBUG
      case 'd': debug_level = atoi(arg(*c));
        if (debug_level > DEBUG) err("unavailable log level\n");
        break;
#endif
      case 'p': procs = atoi(arg(*c)); break;
      case 'm': mem_max = 1024 * atoll(arg(*c)); break;
      case 'c': server_client_max = atoi(arg(*c)); break;
      case 'b': server_buffers_size = atoi(arg(*c)); break;
      case 'q': server_listen_queue = atoi(arg(*c)); break;
      default: err("unknown option\n");
      }
  }
}

uintptr_t map(int prot, int opts, int fd, size_t size)
{
  void *p = mmap(0, size, prot, opts, fd, 0);
  if (p == MAP_FAILED) {
    if (errno == ENOMEM) err("mmap failed, not enough memory\n");
    err("mmap failed, errno=%d\n", errno);
  }
  return (uintptr_t)p;
}

void init()
{
  mem_limit = 1L << (mem_limit_bits < 48 ? mem_limit_bits : 48);
  if (mem_max > mem_limit) mem_max = mem_limit;
  mem_pagesize = getpagesize();
  mem_min = (1 + sizeof(glb) / mem_pagesize) * mem_pagesize;
}

void setup()
{
  inf("arch: wordsize=%d pagesize=%d sem_t=%d pid_t=%d\n", __WORDSIZE,
      (int)mem_pagesize, 8 * (int)sizeof(sem_t), 8 * (int)sizeof(pid_t));
  inf("sizes: addr=%lu slot=%lu reg=%lu quanta=%d\n",
      8 * sizeof(maddr), 8 * sizeof(slot), 8 * sizeof(reg), 8 << ASHIFT);
  int fd_engine = open(engine_path, O_RDWR);
  if (fd_engine == -1)
    err("%s, open failed\n", engine_path);
  struct stat sb;
  if (fstat(fd_engine, &sb) < 0)
    err("%s, stat failed\n", engine_path);
  if (read(fd_engine, (char *)&engine, sizeof(engine_t)) == - 1)
    err("%s, read failed\n", engine_path);
  if (engine.formatid != engine_formatid)
    err("%s, unknown engine format\n", engine_path);
  if (engine.version != engine_version)
    err("%s, unknown engine version\n", engine_path);
  inf("engine: kinds=%d rules=%d (%zuKiB)\n",
      (int)engine.eng_kinds, (int)engine.eng_rules, sb.st_size / 1024);
  eng_ptr = map(PROT_READ, MAP_SHARED, fd_engine, sb.st_size);
  eng_size = sb.st_size;
  eng_table_ptr = eng_ptr + engine.eng_table_offset;
  eng_rules_ptr = eng_ptr + engine.eng_rules_offset;
  if (mem_max > mem_limit)
    err("requested memory cannot be addressed\n");
  inf("slots: reserved=%"PRIu64" max=%"PRIu64" (%zuKiB) \n",
      mem_min / sizeof(slot), mem_max / sizeof(slot), mem_max / 1024);
  int flags = MAP_SHARED | MAP_ANONYMOUS | MAP_NORESERVE;
  mem = map(PROT_READ | PROT_WRITE, flags, -1, mem_max);
  mem_size = mem_max;
  glb = (struct global *)mem;
  glb->slot_base = mem_min >> ASHIFT;
  glb->slot_spawn = mem_min >> ASHIFT;
  glb->slot_limit = mem_max >> ASHIFT;
  reg_format();
  if (sem_init(&glb->sem_procs, 1, 0))
    err("semaphore initialization, errno=%d\n", errno);
  list_init(glb_slots);
  list_init(glb_cuts);
  close(fd_engine);
}

void cleanup()
{
  list_destroy(glb_slots);
  list_destroy(glb_cuts);
  sem_destroy(&glb->sem_procs);
  munmap((void *)mem, mem_size);
  munmap((void *)eng_ptr, eng_size);
}

void down()
{
  server_down();
  if (up) kill(0, SIGTERM);
  while (wait(0) != -1);
  dbg(1, "shutdown\n");
#ifdef DEBUG
  if (glb->stat_client[0]) stats();
#endif
  cleanup();
  dbgc();
}

int main(int argc, char **argv)
{
  init();

  parse(argv);
  if (!engine_path) err("no engine specified\n");
  if (!server_path) err("no socket specified\n");
  if (procs <= 0 || procs > 1000) err("invalid process quantity\n");
  if (mem_max <= mem_min) err("unaffordable memory limit\n");
  if (server_buffers_size < 128) err("insufficient buffers size\n");
  if (server_listen_queue <= 0) err("invalid listen queue size\n");

  setup();

  inf("server: version=%d limit=%d queue=%d buffers=%d \n", server_version,
      server_client_max, server_listen_queue, server_buffers_size);
  inf("workers: procs=%d\n", procs);

  struct sigaction siga;
  siga.sa_flags = 0;
  sigemptyset(&siga.sa_mask);
  siga.sa_handler = sigh;
  sigaction(SIGHUP, &siga, NULL);
  sigaction(SIGTERM, &siga, NULL);
  sigaction(SIGINT, &siga, NULL);
  sigaction(SIGQUIT, &siga, NULL);
  sigaction(SIGTSTP, &siga, NULL);
  sigaction(SIGCONT, &siga, NULL);
  sigaction(SIGPIPE, &siga, NULL);
  sigaction(SIGSEGV, &siga, NULL);

  up = 1;
  setsid();
  workers();
  dbgf("mn");
  dbg(1, "startup\n");
  atexit(down);
  server();

  return 0;
}

