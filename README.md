# Linr

Linr is a lightweight computation server (a dozen hundred lines of C code) which is able to exploit multiprocessor hardware in a natural way.

Compile it with:

    gcc -std=c11 -O3 -lrt -lpthread linr.c -o linr

Run it with 4 worker processes as follows:

    ./linr -p=4 linr.eng linr.socket

Linr is provided along with a binary reduction engine `linr.eng` which describes linear logic reductions.
Tasks can be sent to the server through the socket. A description of the protocol is provided in `linr.tex`.